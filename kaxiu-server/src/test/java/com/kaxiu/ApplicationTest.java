package com.kaxiu;

import com.kaxiu.config.redis.RedisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author: LiYang
 * @create: 2019-08-12 21:20
 * @Description:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {

    @Resource
    private RedisService redisService;


    @Test
    public void contextLoads() {
        redisService.set("testSpringBoot", "valvalvalvla");

        System.out.println(redisService.get("testSpringBoot"));
    }

}
