package com.kaxiu.exception.handle;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by EalenXie on 2019/4/16 15:53.
 */
@Data
public class ExceptionResponse implements Serializable {

    private static final ThreadLocal<ExceptionResponse> EXCEPTION_RESPONSE = new ThreadLocal<>();

    private Integer status;
    private String msg;

}
