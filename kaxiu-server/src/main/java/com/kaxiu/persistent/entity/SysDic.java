package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysDic extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 字典key
     */
    private String dicKey;

    /**
     * 字典value
     */
    private String dicValue;

    /**
     * parent id
     */
    private Long parentId;

    /**
     * 描述
     */
    private String description;


}
