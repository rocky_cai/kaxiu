package com.kaxiu.persistent.entity;

import java.math.BigDecimal;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 提现
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class AccountWithdraw extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 账户编号
     */
    private Long accountId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 提现金额金额
     */
    private BigDecimal amount;

    /**
     * 支付交易id
     */
    private String transactionId;

    /**
     * 审批人id
     */
    private Long approverId;

    /**
     * 审批人姓名
     */
    private String approverName;


}
