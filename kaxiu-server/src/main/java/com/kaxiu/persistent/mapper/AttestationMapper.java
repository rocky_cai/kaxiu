package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.Attestation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 认证记录 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface AttestationMapper extends BaseMapper<Attestation> {

}
