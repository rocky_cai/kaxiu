package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.AccountLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资金账户记录表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface AccountLogMapper extends BaseMapper<AccountLog> {

}
