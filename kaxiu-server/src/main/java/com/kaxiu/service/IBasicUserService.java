package com.kaxiu.service;

import com.kaxiu.persistent.entity.BasicUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IBasicUserService extends IService<BasicUser> {

    BasicUser saveBasicUser(BasicUser user, Integer roleId);

    int bindMobile(String mobile);

    /**
     * 根据手机号查询用户
     * @param currentLoginMobile
     * @return
     */
    BasicUser findByLogin(String currentLoginMobile);

    BasicUser findByOpenId(String openId);
}
