package com.kaxiu.service;

import com.kaxiu.persistent.entity.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色中间表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ISysRoleUserService extends IService<SysRoleUser> {

}
