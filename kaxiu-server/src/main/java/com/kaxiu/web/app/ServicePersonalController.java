package com.kaxiu.web.app;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 维修人员信息表 前端控制器
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@RestController
@RequestMapping("/kaxiu/service-personal")
public class ServicePersonalController {

}

