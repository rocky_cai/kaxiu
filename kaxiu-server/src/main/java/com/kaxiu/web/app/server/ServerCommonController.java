package com.kaxiu.web.app.server;

import com.alibaba.fastjson.JSONObject;
import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.vo.ResultDataWrap;
import org.springframework.web.bind.annotation.*;

/**
 * @author: LiYang
 * @create: 2019-08-15 22:06
 * @Description:
 **/

@RestController
@RequestMapping("/kaxiu/server")
public class ServerCommonController extends AbstractBaseController {


    /**
     * 刷新地理位置
     * @param location
     * @return
     */
    @PostMapping("/location")
    public ResultDataWrap refrshServerLocation(@RequestBody JSONObject location){

        System.out.println(
                location
        );

        return buildResult();
    }

}
