import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
		order:{
			location: {},
			photos: [],
			serviceId: "",
			serviceFee: 0,
		}
    },
	getters: {
		getOrder(state){
			return state.order
		}
	},
    mutations: {
		setLocation(state, location) {
			state.order.location = location
        },
		putPhotos(state, photo){
			state.order.photos.push(photo)
		},
		clearPhotos(state, photo){
			state.order.photos = []
		},
		setServiceId(state, serviceId){
			state.order.serviceId = serviceId
		},
		setServiceFee(state, serviceFee){
			state.order.serviceFee = serviceFee
		}
    }
})

export default store